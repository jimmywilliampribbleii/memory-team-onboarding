<!-- Title: Memory team onboarding: [New team member name] -->

## [First Name], Welcome to GitLab and the Memory Team!

We are all excited that you are joining us on the [Memory Team](https://about.gitlab.com/handbook/engineering/development/enablement/memory/).  You should have already received an onboarding issue from [People Ops](https://about.gitlab.com/handbook/people-operations/) familiarizing yourself with GitLab, setting up accounts, accessing tools, etc.  This onboarding issue is specific to the Memory Team people, processes and setup.

For the first week or so you should focus on your GitLab onboarding issue.  There is a lot of information there, and the first week is very important to get your account information set up correctly.  Tasks from this issue can start as you get ready to start contributing to the Memory team.

Much like your GitLab onboarding issue, each item is broken out by Owner: Action.  Just focus on "New Team Member" items, and feel free to reach out to your team if you have any questions.

### First Day
* [ ] Manager: Invite team member to #g_memory Slack Channel
* [ ] Manager: Invite team member to Thursday weekly Memory Consumption Meeting
* [ ] Manager: Add new team member to [Memory Team Retro](https://gitlab.com/gl-retrospectives/memory-team/-/project_members)

### First Week
* [ ] Manager: Add new team member to [geekbot standup](https://geekbot.com/dashboard/)
* [ ] Manager: Introduce new team member during Gitlab Development Week 1 New Hires Introductions meeting (typically scheduled on Wednesdays)
* [ ] Manager: Add new team member to relevant [office hours](https://gitlab.com/gitlab-org/memory-team/team-tasks/issues/1)
* [ ] New team member: Read about your team, its mission, team members and resources on the [Memory Team page](https://about.gitlab.com/handbook/engineering/development/enablement/memory/)
* [ ] New team member: Set up [coffee chats](https://about.gitlab.com/company/culture/all-remote/tips/#coffee-chats) to meet your team

### Second Week
* [ ] New team member: Read about how Gitlab uses labels [Issue Workflow Labels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md), [Labels CE](https://gitlab.com/gitlab-org/gitlab-ce/labels)
* [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
* [ ] New team member: Familiarize yourself with the team boards
  * [ ] [Memory Team Planning](https://gitlab.com/groups/gitlab-org/-/boards/1077383?label_name[]=Memory)
  * [ ] [Memory Team Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1065668?label_name[]=Memory)
  * [ ] [Memory Team Milestone](https://gitlab.com/groups/gitlab-org/-/boards/1143987?label_name[]=Memory)
  * [ ] [Memory Team by Team Member](https://gitlab.com/groups/gitlab-org/-/boards/1143987?label_name[]=Memory)
* [ ] New team member: Watch Memory Team Overview Videos
  * [ ] [Memory Team 101](https://www.youtube.com/watch?v=enZ9zptATeY&feature=youtu.be)
  * [ ] [Memory Team 102](https://www.youtube.com/watch?v=idQXtzC6QV8&feature=youtu.be)  
  Please note that these videos are set to Private (contain sensitive data), so you need to be logged in as "GitLab Unfiltered" on Youtube to have access.
* [ ] New team member: Please review this onboarding issue and update the template with some improvements as you see fit to make it easier for the next newbie!

### Processes
#### Engineering Workflow

[https://about.gitlab.com/handbook/engineering/workflow/]()

#### Important Dates

* [https://about.gitlab.com/handbook/product/#important-dates-pms-should-keep-in-mind]()
* [https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline]()

#### Workflow Labels

[https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#workflow-labels]()

#### Merge Request Workflow

[https://docs.gitlab.com/ee/development/code_review.html]()

#### Developing with Feature Flags
Consider using feature flags for every medium size-feature:

[https://docs.gitlab.com/ee/development/rolling_out_changes_using_feature_flags.html]()
[https://docs.gitlab.com/ee/development/feature_flags.html]()

#### GitLab CE to GitLab EE merge

[https://docs.gitlab.com/ee/development/automatic_ce_ee_merge.html]()

#### Testing Best Practices

[https://docs.gitlab.com/ee/development/testing_guide/best_practices.html]()

---

#### Getting Started Tasks

Here are a couple tasks that I’ve found that you can take on when you’re ready to start getting in to code. These should be fairly straight forward, but if they turn out to be too complex let me know. These first tasks aren’t intended to be complex features, they are intended to be simple tasks that will help you get familiar the process of shipping code at GitLab.

[add 2 to 3 getting started issues]

/assign @MANAGER @NEWHIRE
/confidential
/due in 21 days
