## Onboarding - Memory Team
This project is intended to aid new team members joining the Memory Team in onboarding.

### Reference links
* [Release Team Onboarding](https://gitlab.com/release-stage/onboarding/blob/master/.gitlab/issue_templates/Release_Onboarding.md)
* [Monitor Team Onboarding](https://gitlab.com/gitlab-org/monitor/onboarding/blob/master/.gitlab/issue_templates/Monitor_Onboarding.md)
* [Description Templates](https://docs.gitlab.com/ee/user/project/description_templates.html)